#!/usr/bin/python

import os

print(f"renew_token.py running! {os.getcwd()}")

if os.path.exists('overrides.json'):
    os.unlink('overrides.json')

with open("overrides.json", mode="x") as fd:
    fd.write('{"headers":{"Authorization":"Token b5638ae7-6e77-4585-b035-7d9de2e3f6b3"}}')

if os.path.exists('overrides.json'):
    print("File exists!")
else:
    print("File doesn't exist!")

